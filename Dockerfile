FROM python:3.9-slim

WORKDIR /moebius

COPY ./pyproject.toml /moebius/pyproject.toml
COPY ./poetry.toml /moebius/poetry.toml
COPY ./poetry.lock /moebius/poetry.lock

RUN pip install poetry
RUN poetry install --no-interaction --no-dev

COPY ./app /moebius/app
CMD ["/moebius/.venv/bin/python", "manage.py", "api", "run"]

EXPOSE 8000