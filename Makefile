CODE = app tests
VENV ?= .venv

TEST = pytest --verbosity=2 --showlocals --log-level=DEBUG

help: ## Show help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

test-db: ## Up database for tests
	docker run --name test-db -e POSTGRES_PASSWORD=test_postgres -e POSTGRES_DB=test_postgres -e POSTGRES_USER=test_postgres -p 5432:5432 -d postgres || true

test: ## Run tests
	$(TEST) -m "not smoke"

test-cov: ## Run tests with coverage report
	$(TEST) --cov -m "not smoke"

test-smoke: ## Run smoke tests
	pytest -v -m smoke

ci-test-cov: ## Run tests with coverage report 
	$(TEST) --cov -m "not smoke"
	coverage xml -o coverage.xml

test-failed: # Run last failed tests
	$(TEST) --last-failed

lint: ## Lint code
	pylint $(CODE)
	mypy $(CODE)
	pydocstyle $(CODE)

format: ## Format all files
	isort $(CODE)
	black $(CODE)

install: ## Install all dependencies
	poetry install --no-interaction

build: ## Build container image
	docker build -t moebius-api .

run: ## Run api service
	uvicorn app.api.factory:create_app --reload

static: ## Static analysis of code
	pylint $(CODE)
	mypy $(CODE)

faust: ## Run Faust
	faust -A app.stream.agents worker -l info 

test-faust: ## Run test Faust:
	faust -A tests.stream.agents worker -l info

fsend: ## send message
	faust -A app.stream.agents send page_views '$(msg)'

gitlab-ci-install: ## Install Gitlab CI/DI runner
	docker run -d \
	--name gitlab-runner \
	-v $(PWD):$(PWD) \
	-v /var/run/docker.sock:/var/run/docker.sock \
	gitlab/gitlab-runner:latest

gitlab-ci-test: ## Run Gitlab Pipelines locally
	docker start gitlab-runner
	docker exec -it -w $(PWD) gitlab-runner gitlab-runner exec docker test
	docker stop gitlab-runner
