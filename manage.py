#!/usr/bin/env python3
import typer
import asyncio
import uvicorn
from typing import Optional

from alembic.config import Config
from alembic import command

from app.api.factory import create_app
from app.db.session import init_db

cli = typer.Typer()
api_cli = typer.Typer()
cli.add_typer(api_cli, name="api")
alembic_cli = typer.Typer()
cli.add_typer(alembic_cli, name="alembic")
db_cli = typer.Typer()
cli.add_typer(db_cli, name="db")

alembic_cfg = Config("app/alembic.ini")


@db_cli.command("init")
def db_init():
    asyncio.run(init_db())
    print("Done")


@api_cli.command("run")
def api(port: int = 8000):
    typer.echo(f"Start API on {port}")
    app = create_app()
    uvicorn.run(app, host="0.0.0.0", port=port)


@alembic_cli.command("makemigrations")
def generate_revision(message: Optional[str] = typer.Argument(None)):
    command.revision(alembic_cfg, message, autogenerate=True)


@alembic_cli.command("upgrade")
def upgrade_revision(name_revision: str = typer.Argument("head")):
    command.upgrade(alembic_cfg, name_revision)


@alembic_cli.command("downgrade")
def downgrade_revision(name_revision: str = typer.Argument('-1')):
    command.downgrade(alembic_cfg, name_revision)


if __name__ == "__main__":
    cli()
