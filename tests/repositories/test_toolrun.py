from unittest.mock import MagicMock, Mock, patch


@patch("app.schemas.toolrun.ToolRun.from_orm", MagicMock())
async def test_create_run(_mocked_toolrun_repo: MagicMock) -> None:
    """Test create_run."""
    run_ = MagicMock()
    await _mocked_toolrun_repo.create_run(run_)
    _mocked_toolrun_repo.session.add.assert_called_once()
    _mocked_toolrun_repo.session.commit.assert_awaited_once()


async def test_delete_run(_mocked_toolrun_repo: MagicMock) -> None:
    """Test delete_run."""
    task_id = Mock()
    await _mocked_toolrun_repo.delete_run(task_id)
    _mocked_toolrun_repo.session.execute.assert_awaited_once()
    _mocked_toolrun_repo.session.commit.assert_awaited_once()


async def test_update_run(_mocked_toolrun_repo: MagicMock) -> None:
    """Test update_run."""
    task_id = Mock()
    run = MagicMock()
    await _mocked_toolrun_repo.update_run(task_id, run)
    _mocked_toolrun_repo.session.execute.assert_awaited_once()
    _mocked_toolrun_repo.session.commit.assert_awaited_once()


async def test_get_all_runs(_mocked_toolrun_repo: MagicMock) -> None:
    """Test get_all_runs."""
    await _mocked_toolrun_repo.get_all_runs()
    _mocked_toolrun_repo.session.execute.assert_awaited_once()
