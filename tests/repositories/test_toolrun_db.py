from _pytest.fixtures import FixtureFunction

from app.repositories.toolrun import ToolRunsRepository
from app.schemas.toolrun import (
    ToolRunCreate,
    ToolRunFilter,
    ToolRunStatus,
    ToolRunUpdate,
)


async def test__get_from_empty_table(
    _prepare_db: FixtureFunction, _toolrun_repo: ToolRunsRepository
) -> None:
    res = await _toolrun_repo.get_all_runs()
    assert len(res) == 0


async def test__create_run(
    _prepare_db: FixtureFunction, _toolrun_repo: ToolRunsRepository
) -> None:
    notebook_id = "a.ipynb"
    res = await _toolrun_repo.create_run(
        ToolRunCreate(task_name=notebook_id, task_id="1")
    )
    assert res.task_name == notebook_id

    all_runs = await _toolrun_repo.get_all_runs()
    assert len(all_runs) == 1

    notebook_id = "b.ipynb"
    res = await _toolrun_repo.create_run(
        ToolRunCreate(task_name=notebook_id, task_id="2")
    )
    assert res.task_name == notebook_id

    all_runs = await _toolrun_repo.get_all_runs()
    assert len(all_runs) == 2


async def test__update_run(
    _prepare_db: FixtureFunction, _toolrun_repo: ToolRunsRepository
) -> None:
    res = await _toolrun_repo.get_all_runs(filters=ToolRunFilter(task_id="1"))

    toolrun = res[0]
    assert toolrun.task_id == "1"
    assert toolrun.result == ToolRunStatus.WAIT.value

    await _toolrun_repo.update_run(
        tool_run_id=1, run=ToolRunUpdate(result=ToolRunStatus.SUCCESS.value)
    )

    res = await _toolrun_repo.get_all_runs(filters=ToolRunFilter(task_run_id=1))

    toolrun = res[0]
    assert toolrun.result == ToolRunStatus.SUCCESS.value


async def test__get_by_nb_id(_toolrun_repo: ToolRunsRepository) -> None:
    res = await _toolrun_repo.get_all_runs(filters=ToolRunFilter(task_name="a.ipynb"))
    assert len(res) == 1
    assert res[0].task_name == "a.ipynb"

    res = await _toolrun_repo.get_all_runs()
    assert len(res) == 2
