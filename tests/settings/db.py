from functools import lru_cache

from app.settings.db import DBSettings


class TestDBSettings(DBSettings):
    """Test Database settings."""

    class Config:
        env_prefix = "TEST_DB_"
        env_file = ".env"


@lru_cache()
def get_test_db_settings() -> TestDBSettings:
    """Return an instance TestDBSettings."""
    return TestDBSettings()
