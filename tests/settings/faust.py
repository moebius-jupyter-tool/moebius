from functools import lru_cache

from app.settings.faust import FaustSettings


class TestFaustSettings(FaustSettings):
    """Test Faust settings."""

    @property
    def test_brocker_uri(self) -> str:
        """Get test brocker uri."""
        return f"{self.host}:{self.port}"

    class Config:
        env_prefix = "TEST_FAUST_"
        env_file = ".env"


@lru_cache()
def get_test_faust_settings() -> TestFaustSettings:
    """Return an instance TestFaustSettings."""
    return TestFaustSettings()
