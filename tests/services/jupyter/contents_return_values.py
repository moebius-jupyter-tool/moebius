DIRECTORY_CONTENT = {
    "path": "work",
    "content": [
        {
            "name": "evaluated",
            "path": "work/evaluated",
            "content": None,
            "type": "directory",
        },
        {
            "name": "test.ipynb",
            "path": "work/test.ipynb",
            "content": None,
            "type": "notebook",
        },
        {
            "name": "test2.ipynb",
            "path": "work/test2.ipynb",
            "content": None,
            "type": "notebook",
        },
    ],
    "format": "json",
    "type": "directory",
}


SUCCESS_NOTEBOOKS_CONTENT = {
    "name": "test.ipynb",
    "path": "test.ipynb",
    "content": {
        "cells": [
            {
                "cell_type": "code",
                "execution_count": None,
                "outputs": [],
                "source": 'print("Test notebook")',
            }
        ]
    },
    "format": "json",
    "type": "notebook",
}

ERROR_NOTEBOOK_CONTENT = {
    "name": "test.ipynb",
    "path": "test.ipynb",
    "content": {
        "cells": [
            {
                "cell_type": "code",
                "execution_count": None,
                "outputs": [],
                "source": "1 / 0",
            }
        ]
    },
    "format": "json",
    "type": "notebook",
}
