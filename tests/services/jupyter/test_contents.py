from unittest.mock import patch

from httpx import AsyncClient

import app.services.jupyter.utils.contents as contents_module
from tests.services.jupyter.contents_return_values import (
    DIRECTORY_CONTENT,
    SUCCESS_NOTEBOOKS_CONTENT,
)


@patch(
    "app.services.jupyter.utils.contents.get_contents", return_value=DIRECTORY_CONTENT
)
async def test__get_contents_of_directory(_client: AsyncClient) -> None:
    contents = await contents_module.get_contents(path="work", client=_client)

    assert contents["type"] == "directory"
    assert contents["path"] == "work"


@patch(
    "app.services.jupyter.utils.contents.get_contents",
    return_value=SUCCESS_NOTEBOOKS_CONTENT,
)
async def test__get_contents_of_notebook(_client: AsyncClient) -> None:
    contents = await contents_module.get_contents(
        path="work/test.ipynb", client=_client
    )

    assert contents["type"] == "notebook"
    assert contents["name"] == "test.ipynb"
