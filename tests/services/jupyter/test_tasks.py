from _pytest.fixtures import FixtureFunction

from app.schemas.toolrun import ToolRunCreate, ToolRunFilter, ToolRunStatus
from app.services.tasks import create_toolrun, update_toolrun_status
from app.units.toolrun import RunnerToolRunUnit, ToolRunUnit


async def test__create_toolrun(
    _prepare_db: FixtureFunction, _toolrun_unit: ToolRunUnit
) -> None:
    task_name = "work/b.ipynb"
    unit = RunnerToolRunUnit(notebook_path=task_name)
    unit.repository = _toolrun_unit.repository

    result = await unit.repository.get_all_runs(
        filters=ToolRunFilter(task_name=task_name)
    )
    assert len(result) == 0

    await create_toolrun(
        unit=unit,
        run=ToolRunCreate(task_name=task_name, task_id="1"),
    )

    result = await unit.repository.get_all_runs(
        filters=ToolRunFilter(task_name=task_name)
    )
    assert len(result) == 1


async def test__save_toolrun_status(
    _prepare_db: FixtureFunction, _toolrun_unit: ToolRunUnit
) -> None:
    task_name = "work/a.ipynb"
    unit = RunnerToolRunUnit(notebook_path=task_name)
    unit.repository = _toolrun_unit.repository

    toolrun = await unit.repository.create_run(
        run=ToolRunCreate(task_name=task_name, task_id="1")
    )

    assert toolrun.result == ToolRunStatus.WAIT.value

    await update_toolrun_status(unit=unit, status=ToolRunStatus.SUCCESS)

    all_runs = await unit.repository.get_all_runs(
        filters=ToolRunFilter(task_name=task_name)
    )

    assert all_runs[0].result == ToolRunStatus.SUCCESS.value
