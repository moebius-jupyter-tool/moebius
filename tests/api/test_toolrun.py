from unittest.mock import AsyncMock, patch

from httpx import AsyncClient

from app.schemas.toolrun import ToolRunCreate

test_data = [
    {
        "task_id": "task_id",
        "notebook_id": "notebook1",
        "result": "waiting",
        "start_dttm": "2022-04-10T20:00:15.769642",
        "end_dttm": None,
    }
]

test_data_update = {"result": "success", "end_dttm": "2022-04-10T20:00:15.769642"}

test_data_create = ToolRunCreate(task_name="notebook1", task_id="task_id")


@patch(
    "app.repositories.toolrun.ToolRunsRepository.create_run",
    AsyncMock(return_value=test_data),
)
async def test_create(_client: AsyncClient) -> None:
    """Test create_run."""

    res = await _client.post("/toolruns", json=test_data_create.dict())
    assert res.status_code == 201
    assert res.json() == test_data


@patch(
    "app.repositories.toolrun.ToolRunsRepository.get_all_runs",
    AsyncMock(return_value=test_data),
)
async def test_get_all_runs(_client: AsyncClient) -> None:
    """Test get_all_runs."""

    res = await _client.get("/toolruns")
    assert res.status_code == 200
    assert res.json() == test_data


@patch(
    "app.repositories.toolrun.ToolRunsRepository.get_all_runs",
    AsyncMock(return_value=test_data),
)
@patch(
    "app.repositories.toolrun.ToolRunsRepository.update_run", AsyncMock(return_value=1)
)
async def test_update_run(_client: AsyncClient) -> None:
    """Test update_run."""

    res = await _client.put("/toolruns/1/", json=test_data_update)
    assert res.status_code == 200
    assert res.json() == test_data[0]


@patch(
    "app.repositories.toolrun.ToolRunsRepository.get_all_runs",
    AsyncMock(return_value=test_data),
)
@patch(
    "app.repositories.toolrun.ToolRunsRepository.delete_run", AsyncMock(return_value=1)
)
async def test_delete_run(_client: AsyncClient) -> None:
    """Test delete_run."""

    res = await _client.delete("/toolruns/1/")
    assert res.status_code == 200
    assert res.json() == test_data[0]
