import aiokafka
import pytest
from _pytest.fixtures import FixtureFunction

from app.stream.codec import register_codecs
from app.stream.models import ToolActionRecord
from tests.settings.faust import get_test_faust_settings

settings = get_test_faust_settings()


@pytest.mark.smoke
async def test__send_and_wait_result(_prepare_db: FixtureFunction) -> None:
    register_codecs()

    raw_message = {
        "task_id": "task_id",
        "task_name": "work/success_nb.ipynb",
        "tool_run_id": 1,
        "action": "run",
    }

    tool_action = ToolActionRecord(**raw_message)  # type: ignore
    byte_message = tool_action.dumps()

    producer = aiokafka.AIOKafkaProducer(bootstrap_servers=settings.test_brocker_uri)
    consumer = aiokafka.AIOKafkaConsumer(
        "tool_result", bootstrap_servers=settings.test_brocker_uri
    )

    await producer.start()
    await consumer.start()

    await producer.send_and_wait("tool_action", byte_message)

    await consumer.getone()

    await producer.stop()
    await consumer.stop()
