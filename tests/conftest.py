import asyncio
from typing import AsyncGenerator, Generator
from unittest.mock import AsyncMock, MagicMock

import pytest
from fastapi import FastAPI
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession

from app.api.factory import create_app
from app.models.base import Base
from app.repositories.toolrun import ToolRunsRepository
from app.settings.api import get_api_settings
from app.units.toolrun import ToolRunUnit
from tests.db_test import async_session, engine


@pytest.fixture(scope="session")
def event_loop() -> Generator:
    """Get event loop."""

    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture
def _app() -> FastAPI:
    return create_app()


@pytest.fixture
async def _client(_app: FastAPI) -> AsyncGenerator[AsyncClient, None]:
    settings = get_api_settings()
    async with AsyncClient(app=_app, base_url=settings.base_url) as client:
        yield client


@pytest.fixture
def _session() -> AsyncSession:
    result = MagicMock()
    attrs = {"execute.return_value": result}
    session = AsyncMock(AsyncSession(), **attrs)
    return session


@pytest.fixture
def _mocked_toolrun_repo(_session: AsyncMock) -> ToolRunsRepository:
    return ToolRunsRepository(_session)


@pytest.fixture(scope="session")
async def _prepare_db() -> AsyncGenerator:
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)

    yield

    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)


@pytest.fixture
async def _get_session() -> AsyncGenerator[AsyncSession, None]:
    """Get test session."""
    async with async_session() as db:
        yield db


@pytest.fixture
def _toolrun_repo(_get_session: AsyncSession) -> ToolRunsRepository:
    return ToolRunsRepository(_get_session)


@pytest.fixture
def _toolrun_unit(_toolrun_repo: ToolRunsRepository) -> ToolRunUnit:
    unit = ToolRunUnit(session_factory=async_session)
    unit.session = _toolrun_repo.session
    unit.repository = _toolrun_repo

    return unit
