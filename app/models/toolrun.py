from sqlalchemy import Column, DateTime, Integer, String, func

from app.models.base import Base
from app.schemas.toolrun import ToolRunStatus


class ToolRun(Base):
    """ToolRun database model."""

    __tablename__ = "toolrun"
    tool_run_id = Column(Integer, autoincrement=True, primary_key=True)
    task_id = Column(String)
    task_name = Column(String)
    result = Column(String, default=ToolRunStatus.WAIT.value)
    start_dttm = Column(DateTime(timezone=True), default=func.now())
    end_dttm = Column(DateTime(timezone=True), nullable=True, default=None)
