from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

from app.models.base import Base
from app.settings.db import get_db_settings

settings = get_db_settings()
DATABASE_URL = settings.db_url

engine = create_async_engine(DATABASE_URL, future=True)
async_session = sessionmaker(engine, class_=AsyncSession)


async def init_db() -> None:
    """Initialize database."""

    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
