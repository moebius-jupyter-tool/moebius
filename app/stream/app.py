# pylint: skip-file
from typing import Any

import faust

from app.settings.faust import get_faust_settings
from app.stream.codec import register_codecs

settings = get_faust_settings()
app = faust.App(settings.app_id, broker=settings.broker_uri)


@app.on_after_configured.connect  # type: ignore
def after_configuration(app: faust.App, **kwargs: Any) -> None:  # type: ignore
    """After Faust app configured."""
    register_codecs()
