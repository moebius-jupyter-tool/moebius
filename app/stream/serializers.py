from faust.serializers.codecs import Codec
from schema_registry.client import SchemaRegistryClient
from schema_registry.serializers.faust import FaustSerializer


def get_schema_registry_codec(client: SchemaRegistryClient, subject: str) -> Codec:
    """Codec factory."""
    result = client.get_schema(subject)
    return FaustSerializer(client, subject, result.schema)
