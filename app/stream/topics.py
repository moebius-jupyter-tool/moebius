from app.stream.app import app
from app.stream.models import ToolActionRecord, ToolResultRecord

tool_action_topic = app.topic("tool_action", value_type=ToolActionRecord)
tool_result_topic = app.topic("tool_result", value_type=ToolResultRecord)
