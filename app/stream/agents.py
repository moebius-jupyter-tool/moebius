import logging

from faust.types import StreamT

from app.services.tasks import delete_dead_runs_task, notebook_runner_task
from app.settings.faust import get_faust_settings
from app.stream.app import app
from app.stream.models import TaskResultTypes
from app.stream.topics import tool_action_topic, tool_result_topic
from app.stream.utils import (
    create_message_sender,
    prepare_toolresult_record,
    prepare_toolrun,
)
from app.units.toolrun import RunnerToolRunUnit, ToolRunUnit

logger = logging.getLogger(__name__)
settings = get_faust_settings()


@app.agent(tool_action_topic)
async def tool_action(messages: StreamT) -> None:
    """Tool action agent."""

    async for message in messages:
        logger.info("Event received in topic tool_action")

        async with RunnerToolRunUnit(notebook_path=message.task_name) as unit:
            parsed_message = message.asdict()
            result_topic_sender = create_message_sender(topic=tool_result_topic)

            if unit.error:
                parsed_message["result"] = TaskResultTypes.ERROR
                message_data = prepare_toolresult_record(data=parsed_message)

                await result_topic_sender(message_data=message_data)
            else:
                await notebook_runner_task(
                    unit=unit,
                    toolrun=prepare_toolrun(parsed_message),
                    topic_sender=result_topic_sender,
                )


@app.agent(tool_result_topic)
async def tool_result(messages: StreamT) -> None:
    """Tool result agent."""

    async for _ in messages:
        logger.info("Event received in topic tool_result")


@app.timer(interval=settings.cleanup_interval)
async def delete_dead_toolruns() -> None:
    """Delete dead runs."""
    async with ToolRunUnit() as unit:
        result_topic_sender = create_message_sender(topic=tool_result_topic)
        await delete_dead_runs_task(unit=unit, topic_sender=result_topic_sender)
        logger.info("Task delete_dead_runs_task finished")
