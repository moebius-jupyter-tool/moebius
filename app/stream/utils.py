from typing import Any, Callable, Dict

from faust import TopicT

from app.schemas.toolrun import ToolRunCreate
from app.stream.models import ToolResultRecord


def create_message_sender(topic: TopicT) -> Callable:
    """Send_message factory."""

    async def send_message(message_data: Any) -> None:
        """Send message to topic."""
        await topic.send(value=message_data)

    return send_message


def prepare_toolrun(parsed_message: Dict) -> ToolRunCreate:
    """Prepare toolrun for tasks."""

    return ToolRunCreate(
        task_name=parsed_message["task_name"],
        task_id=parsed_message["task_id"],
    )


def prepare_toolresult_record(data: Dict) -> ToolResultRecord:
    """Create result record from toolrun."""
    return ToolResultRecord.from_data(data=data)
