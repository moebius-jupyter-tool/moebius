from abc import ABCMeta
from datetime import datetime
from enum import Enum
from typing import Optional

import faust


class TaskResultTypes(str, Enum):
    """Типы результатов TaskRun."""

    SUCCESS = "success"
    ERROR = "error"
    WAITING = "waiting"


class ToolActionRecord(
    faust.Record, metaclass=ABCMeta, serializer="TOOL_ACTION_SERIALIZER"
):
    """Сообщение из топика ToolAction, содержащее действия, которые должен выполнить Tool."""

    task_id: str
    task_name: str
    tool_run_id: int
    action: str


class ToolResultRecord(
    faust.Record, metaclass=ABCMeta, serializer="TOOL_RESULT_SERIALIZER"
):
    """Сообщение из топика ToolResult, содержащее результаты работы Tool."""

    task_id: str
    task_name: str
    tool_run_id: int
    result: TaskResultTypes
    start_dttm: Optional[datetime]
    end_dttm: Optional[datetime]
