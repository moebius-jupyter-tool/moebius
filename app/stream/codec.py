import faust
from schema_registry.client import SchemaRegistryClient

from app.settings.faust import get_faust_settings
from app.stream.serializers import get_schema_registry_codec

settings = get_faust_settings()
client = SchemaRegistryClient(url=settings.schema_registry_url)

CODEC_SUBJECT = (
    ("TOOL_ACTION_SERIALIZER", "TOOL_ACTION"),
    ("TOOL_RESULT_SERIALIZER", "TOOL_RESULT"),
)


def register_codecs() -> None:
    """Register codecs."""
    for codec_name, subject in CODEC_SUBJECT:
        codec = get_schema_registry_codec(client, subject)
        faust.serializers.codecs.register(codec_name, codec)
