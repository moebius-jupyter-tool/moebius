from typing import Optional

from pydantic import BaseModel


class Notebook(BaseModel):
    """Notebook model."""

    path: Optional[str] = None
    name: str
