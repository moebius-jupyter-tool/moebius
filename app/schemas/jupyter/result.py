from enum import Enum

from pydantic import BaseModel


class ExecutionStatus(Enum):
    """Execution status."""

    SUCCESS = "success"
    FAILURE = "failure"


class RunResult(BaseModel):
    """Run result model."""

    status: ExecutionStatus
