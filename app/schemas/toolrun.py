from datetime import datetime
from enum import Enum
from typing import Optional

from pydantic import BaseModel


class ToolRunStatus(Enum):
    """ToolRun status."""

    SUCCESS = "success"
    ERROR = "error"
    WAIT = "wait"


class ToolRunBase(BaseModel):
    """ToolRunsBase schema."""

    tool_run_id: int
    task_id: str
    task_name: str
    result: str
    start_dttm: datetime
    end_dttm: Optional[datetime] = None


class ToolRunCreate(BaseModel):
    """ToolRunCreate schema."""

    task_id: str
    task_name: str


class ToolRunUpdate(BaseModel):
    """ToolRunsUpdate schema."""

    result: Optional[str]
    end_dttm: Optional[datetime]


class ToolRun(ToolRunBase):
    """ToolRuns schema."""

    class Config:
        orm_mode = True


class ToolRunFilter(BaseModel):
    """Filter schema."""

    tool_run_id: Optional[int]
    task_id: Optional[str]
    task_name: Optional[str]
    result: Optional[str]
    start_dttm: Optional[datetime]
    end_dttm: Optional[datetime]
