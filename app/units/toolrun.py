from types import TracebackType
from typing import Type

from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import sessionmaker

from app.db.session import async_session
from app.repositories.toolrun import ToolRunsRepository
from app.services.jupyter.exceptions import InvalidPathException
from app.services.jupyter.notebook import Notebook
from app.settings.jupyter import JupyterSettings, get_jupyter_settings
from app.units.base import BaseUnit


class ToolRunUnit(BaseUnit):
    """ToolRun work unit."""

    def __init__(self, session_factory: sessionmaker = async_session) -> None:
        super().__init__(session_factory=session_factory)

        self.session: AsyncSession
        self.repository: ToolRunsRepository
        self.settings: JupyterSettings

    async def __aenter__(self) -> "ToolRunUnit":
        self.session = self.session_factory()
        self.repository = ToolRunsRepository(session=self.session)
        self.settings = get_jupyter_settings()

        return self

    async def __aexit__(
        self,
        exc_type: Type[BaseException],
        exc_value: BaseException,
        traceback: TracebackType,
    ) -> None:
        await self.session.close()


class RunnerToolRunUnit(ToolRunUnit):
    """RunnerToolRunUnit work unit."""

    def __init__(
        self, notebook_path: str, session_factory: sessionmaker = async_session
    ) -> None:
        super().__init__(session_factory)

        self.notebook: Notebook = Notebook(path=notebook_path)
        self.error: bool = False

    async def __aenter__(self) -> "RunnerToolRunUnit":
        await super().__aenter__()

        try:
            await self.notebook.read_remote(base_url=self.settings.api_url)
        except InvalidPathException:
            self.error = True

        return self
