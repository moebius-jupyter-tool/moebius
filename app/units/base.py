from types import TracebackType
from typing import Type

from sqlalchemy.orm import sessionmaker

from app.db.session import async_session


class BaseUnit:
    """Base work unit class."""

    def __init__(self, session_factory: sessionmaker = async_session) -> None:
        self.session_factory = session_factory

    async def __aenter__(self) -> "BaseUnit":
        return self

    async def __aexit__(
        self,
        exc_type: Type[BaseException],
        exc_value: BaseException,
        traceback: TracebackType,
    ) -> None:
        raise NotImplementedError
