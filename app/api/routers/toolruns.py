from typing import List, Optional

from fastapi import APIRouter, Depends
from starlette.exceptions import HTTPException
from starlette.status import HTTP_201_CREATED, HTTP_404_NOT_FOUND

from app.api.dependencies import get_repository
from app.repositories.toolrun import ToolRunsRepository
from app.schemas import toolrun

router = APIRouter(prefix="/toolruns", tags=["toolruns"])


@router.get("")
async def get_all_runs(
    fil: Optional[toolrun.ToolRunFilter] = Depends(toolrun.ToolRunFilter),
    lim: Optional[int] = None,
    repo: ToolRunsRepository = Depends(get_repository(ToolRunsRepository)),
) -> List[toolrun.ToolRun]:
    """Read all runs."""
    db_run = await repo.get_all_runs(fil, lim)
    return db_run


@router.post("", status_code=HTTP_201_CREATED)
async def create_run(
    run: toolrun.ToolRunCreate,
    repo: ToolRunsRepository = Depends(get_repository(ToolRunsRepository)),
) -> toolrun.ToolRun:
    """Create new run."""
    db_run = await repo.create_run(run)
    return db_run


@router.put("/{tool_run_id}/")
async def update_run(
    tool_run_id: int,
    run: toolrun.ToolRunUpdate,
    repo: ToolRunsRepository = Depends(get_repository(ToolRunsRepository)),
) -> toolrun.ToolRun:
    """Update run."""
    db_run = await repo.get_all_runs(
        filters=toolrun.ToolRunFilter(tool_run_id=tool_run_id)
    )
    if not db_run:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND)

    await repo.update_run(tool_run_id, run)
    return db_run[0]


@router.delete("/{tool_run_id}/")
async def delete_run(
    tool_run_id: int,
    repo: ToolRunsRepository = Depends(get_repository(ToolRunsRepository)),
) -> toolrun.ToolRun:
    """Delete run."""
    db_run = await repo.get_all_runs(
        filters=toolrun.ToolRunFilter(task_run_id=tool_run_id)
    )
    if not db_run:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND)

    await repo.delete_run(tool_run_id)
    return db_run[0]
