from pydantic import BaseModel


class BaseAPIResponse(BaseModel):
    """Base API response scheme."""

    details: str
