from typing import AsyncGenerator, Callable, Type

from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession

from app.db.session import async_session
from app.repositories.base import BaseRepository


async def get_db() -> AsyncGenerator[AsyncSession, None]:
    """Get database session."""
    async with async_session() as db:
        yield db


def get_repository(
    repo: Type[BaseRepository],
) -> Callable[[AsyncSession], BaseRepository]:
    """Get repository instance."""

    def _get_repo(db: AsyncSession = Depends(get_db)) -> BaseRepository:
        return repo(db)

    return _get_repo
