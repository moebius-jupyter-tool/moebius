from fastapi import FastAPI

from app.api.routers import ROUTERS


def create_app() -> FastAPI:
    """Create a FastAPI app."""
    app = FastAPI()

    for route in ROUTERS:
        app.include_router(route)

    return app
