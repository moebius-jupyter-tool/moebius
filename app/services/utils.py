from typing import Dict, List, Optional

from app.schemas.jupyter.session import Session
from app.schemas.toolrun import ToolRun


def get_toolruns_of_session(
    active_sessions: List[Session], waiting_toolruns: List[ToolRun]
) -> Dict[Optional[Session], List[ToolRun]]:
    """Return dict of sessions and corresponding toolruns."""
    toolruns_of_session: Dict[Optional[Session], List[ToolRun]] = {None: []}

    for active_session in active_sessions:
        toolruns_of_session[active_session] = []

    for toolrun in waiting_toolruns:
        corresponding_session = None
        for session in active_sessions:
            if session.path == toolrun.task_name:
                corresponding_session = session

        toolruns_of_session[corresponding_session].append(toolrun)

    return toolruns_of_session
