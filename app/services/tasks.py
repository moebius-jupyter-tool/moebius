from datetime import datetime
from typing import Callable, Optional

from app.schemas.jupyter.result import ExecutionStatus
from app.schemas.toolrun import (
    ToolRun,
    ToolRunCreate,
    ToolRunFilter,
    ToolRunStatus,
    ToolRunUpdate,
)
from app.services.jupyter.runner import NotebookRunner
from app.services.jupyter.utils.client import create_client
from app.services.jupyter.utils.session import delete_session, get_sessions
from app.services.utils import get_toolruns_of_session
from app.stream.utils import prepare_toolresult_record
from app.units.toolrun import RunnerToolRunUnit, ToolRunUnit


async def update_toolrun_status(
    unit: RunnerToolRunUnit, status: ToolRunStatus
) -> ToolRun:
    """Update the status of the notebook."""

    toolruns = await unit.repository.get_all_runs(
        filters=ToolRunFilter(task_name=unit.notebook.path)
    )

    tool_run_id = toolruns[-1].tool_run_id

    await unit.repository.update_run(
        tool_run_id=tool_run_id,
        run=ToolRunUpdate(result=status.value, end_dttm=datetime.now()),
    )

    updated_toolrun = await unit.repository.get_all_runs(
        filters=ToolRunFilter(task_name=unit.notebook.path)
    )
    return updated_toolrun[-1]


async def create_toolrun(unit: RunnerToolRunUnit, run: ToolRunCreate) -> None:
    """Create toolrun."""
    await unit.repository.create_run(run=run)


async def notebook_runner_task(
    unit: RunnerToolRunUnit, toolrun: ToolRunCreate, topic_sender: Callable
) -> None:
    """Notebook background task."""

    async with NotebookRunner(notebook=unit.notebook) as runner:
        await create_toolrun(unit=unit, run=toolrun)

        try:
            result = await runner.execute_all_cells()

            toolrun_status = (
                ToolRunStatus.SUCCESS
                if result.status == ExecutionStatus.SUCCESS
                else ToolRunStatus.ERROR
            )
        except RuntimeError:
            toolrun_status = ToolRunStatus.ERROR

        toolrun_data = await update_toolrun_status(unit=unit, status=toolrun_status)
        tool_result_record = prepare_toolresult_record(toolrun_data.dict())

        await topic_sender(message_data=tool_result_record)


async def delete_dead_runs_task(
    unit: ToolRunUnit, topic_sender: Optional[Callable] = None
) -> None:
    """Delete dead runs."""
    client = create_client(base_url=unit.settings.api_url)

    active_sessions = await get_sessions(client=client)
    waiting_toolruns = await unit.repository.get_all_runs(
        filters=ToolRunFilter(result=ToolRunStatus.WAIT.value)
    )

    toolruns_of_session = get_toolruns_of_session(
        active_sessions=active_sessions, waiting_toolruns=waiting_toolruns
    )

    for session, toolruns in toolruns_of_session.items():
        if session and session.kernel.execution_state == "idle" or not session:
            for toolrun in toolruns:
                await unit.repository.update_run(
                    tool_run_id=toolrun.tool_run_id,
                    run=ToolRunUpdate(
                        result=ToolRunStatus.ERROR.value, end_dttm=datetime.now()
                    ),
                )
                run_data = await unit.repository.get_all_runs(
                    filters=ToolRunFilter(tool_run_id=toolrun.tool_run_id)
                )
                if topic_sender:
                    tool_result_record = prepare_toolresult_record(run_data[-1].dict())
                    await topic_sender(message_data=tool_result_record)

            if session:
                await delete_session(session_id=session.id, client=client)
