from enum import Enum
from types import TracebackType
from typing import Callable, Optional, Type, TypeVar

from httpx import AsyncClient, HTTPError
from websockets.exceptions import WebSocketException

from app.schemas.jupyter.result import RunResult
from app.schemas.jupyter.session import CreateSession, Session
from app.services.jupyter.exceptions import SessionExists
from app.services.jupyter.kernel import Kernel
from app.services.jupyter.notebook import Notebook
from app.services.jupyter.utils.client import create_client
from app.services.jupyter.utils.session import create_session, delete_session

TNotebookRunner = TypeVar("TNotebookRunner", bound="NotebookRunner")


class RunnerState(Enum):
    """Runner class state."""

    UNOPENED = 1
    OPENED = 2
    CLOSED = 3


class NotebookRunner:
    """Notebook runner class."""

    def __init__(
        self,
        *,
        notebook: Notebook,
        on_notebook_start: Optional[Callable] = None,
        on_notebook_end: Optional[Callable] = None,
        host: str = "127.0.0.1",
        port: int = 8888,
    ) -> None:
        self._state: RunnerState = RunnerState.UNOPENED

        self.notebook: Notebook = notebook

        self.host: str = host
        self.port: int = port

        self.on_notebook_start = on_notebook_start
        self.on_notebook_finish = on_notebook_end

        self._client: AsyncClient = create_client(
            base_url=f"http://{self.host}:{self.port}/api"
        )

        self._session: Session
        self._kernel: Kernel

    async def execute_all_cells(self) -> RunResult:
        """Execute all cells."""
        if self._state != RunnerState.OPENED:
            raise RuntimeError("Create NotebookRunner instance first.")

        if self.on_notebook_start:
            await self.on_notebook_start()

        try:
            run_result = await self._kernel.execute(cells=self.notebook.cells)
        except WebSocketException as e:
            raise RuntimeError from e

        if self.on_notebook_finish:
            await self.on_notebook_finish()

        return run_result

    async def __aenter__(self: TNotebookRunner) -> TNotebookRunner:
        if self._state != RunnerState.UNOPENED:
            raise RuntimeError(
                "Cannot create a NotebookRunner instance more than once.",
            )

        try:
            self._session = await create_session(
                session_data=CreateSession(
                    name=self.notebook.name, path=self.notebook.path
                ),
                client=self._client,
            )
            self._kernel = Kernel(session=self._session)
            await self._kernel.start(base_url=f"{self.host}:{self.port}/api")
            self._state = RunnerState.OPENED
        except (HTTPError, WebSocketException, SessionExists):
            self._state = RunnerState.CLOSED

        return self

    async def __aexit__(
        self,
        exc_type: Type[BaseException],
        exc_value: BaseException,
        traceback: TracebackType,
    ) -> Optional[bool]:

        try:
            if self._state == RunnerState.OPENED:
                await delete_session(session_id=self._session.id, client=self._client)
                self._state = RunnerState.CLOSED
            return None
        except HTTPError:
            return True
