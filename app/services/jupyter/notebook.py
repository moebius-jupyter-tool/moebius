from typing import Dict, List

from app.services.jupyter.utils.client import create_client
from app.services.jupyter.utils.contents import get_contents


class Notebook:
    """JupyterNotebook class."""

    def __init__(self, *, path: str) -> None:
        self.path: str = path

        self.name: str
        self.cells: List[Dict] = []

    async def read_remote(self, base_url: str) -> None:
        """Open remote notebook."""

        client = create_client(base_url=base_url)

        nb_content = await get_contents(path=self.path, client=client)
        self.cells = nb_content["content"]["cells"]

        notebook_name = self.path.split("/")[-1]
        self.name = notebook_name
