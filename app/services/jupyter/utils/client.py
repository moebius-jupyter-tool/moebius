from typing import Any

from httpx import AsyncClient


def create_client(**kwargs: Any) -> AsyncClient:
    """Create AsyncClient."""
    return AsyncClient(**kwargs)
