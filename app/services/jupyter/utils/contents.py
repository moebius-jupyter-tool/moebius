from typing import Dict

from fastapi import status
from httpx import AsyncClient

from app.services.jupyter.exceptions import InvalidPathException
from app.settings.jupyter import get_jupyter_settings

settings = get_jupyter_settings()


async def get_contents(path: str, client: AsyncClient) -> Dict:
    """Get content by path."""
    url = f"{settings.api_url}/contents/{path}"

    response = await client.get(url)

    if response.status_code == status.HTTP_404_NOT_FOUND:
        raise InvalidPathException(f"No such file or directory: {path}")

    return response.json()
