from typing import List, Optional

from sqlalchemy import delete, update
from sqlalchemy.future import select

import app.models.toolrun as models
import app.schemas.toolrun as schemas
from app.repositories.base import BaseRepository


class ToolRunsRepository(BaseRepository):
    """ToolRuns repository class."""

    async def create_run(self, run: schemas.ToolRunCreate) -> schemas.ToolRun:
        """Create new run."""
        new_run = models.ToolRun(task_name=run.task_name, task_id=run.task_id)
        self.session.add(new_run)
        await self.session.commit()
        await self.session.refresh(new_run)
        res = schemas.ToolRun.from_orm(new_run)
        return res

    async def get_all_runs(
        self,
        filters: Optional[schemas.ToolRunFilter] = None,
        lim: Optional[int] = None,
    ) -> List[schemas.ToolRun]:
        """Read all runs."""
        filters = filters or schemas.ToolRunFilter()

        q = await self.session.execute(
            select(models.ToolRun)
            .order_by(models.ToolRun.tool_run_id)
            .limit(lim)
            .filter_by(**filters.dict(exclude_none=True))
        )

        all_runs_models = q.scalars().all()
        all_runs_schemas = [
            schemas.ToolRun.from_orm(run_model) for run_model in all_runs_models
        ]

        return all_runs_schemas

    async def update_run(self, tool_run_id: int, run: schemas.ToolRunUpdate) -> None:
        """Update run."""
        q = update(models.ToolRun).where(models.ToolRun.tool_run_id == tool_run_id)
        q = q.values(**run.dict())

        await self.session.execute(q)
        await self.session.commit()

    async def delete_run(self, tool_run_id: int) -> None:
        """Delete run."""
        q = delete(models.ToolRun).where(models.ToolRun.tool_run_id == tool_run_id)

        await self.session.execute(q)
        await self.session.commit()
