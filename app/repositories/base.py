from sqlalchemy.ext.asyncio import AsyncSession


class BaseRepository:
    """Base repository class."""

    def __init__(self, session: AsyncSession) -> None:
        """Initialize repository."""
        self._session = session

    @property
    def session(self) -> AsyncSession:
        """Return Connection instance."""
        return self._session
