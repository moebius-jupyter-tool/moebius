from functools import lru_cache

from pydantic import BaseSettings


class DBSettings(BaseSettings):
    """Database settings."""

    username: str
    password: str
    database: str
    host: str
    port: str

    @property
    def db_url(self) -> str:
        """Get database URL."""
        return (
            f"postgresql+asyncpg://{self.username}:"
            f"{self.password}@{self.host}:{self.port}/{self.database}"
        )

    class Config:
        env_prefix = "DB_"
        env_file = ".env"


@lru_cache()
def get_db_settings() -> DBSettings:
    """Return an instance DBSettings."""
    return DBSettings()
