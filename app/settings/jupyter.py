from functools import lru_cache

from pydantic import BaseSettings


class JupyterSettings(BaseSettings):
    """Jupyter Notebook settings."""

    host: str
    port: int
    launch_limit_seconds: int

    @property
    def api_url(self) -> str:
        """Get jupyter server URL."""
        return f"http://{self.host}:{self.port}/api"

    @property
    def ws_url(self) -> str:
        """Get websockets URL."""
        return f"ws://{self.host}:{self.port}/api"

    class Config:
        env_prefix = "JUPYTER_"
        env_file = ".env"


@lru_cache()
def get_jupyter_settings() -> JupyterSettings:
    """Return an instance DBSettings."""
    return JupyterSettings()
