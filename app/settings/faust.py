from functools import lru_cache

from pydantic import BaseSettings


class FaustSettings(BaseSettings):
    """Faust settings."""

    host: str
    port: int
    partitions: int
    app_id: str
    schema_registry_url: str
    cleanup_interval: int

    @property
    def broker_uri(self) -> str:
        """Get broker URI."""
        return f"kafka://{self.host}:{self.port}"

    class Config:
        env_prefix = "FAUST_"
        env_file = ".env"


@lru_cache()
def get_faust_settings() -> FaustSettings:
    """Return an instance FaustSettings."""
    return FaustSettings()
