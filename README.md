# Moebius Jupyter-Tool ∞
Сервис для запуска Jupyter Notebooks.
## Команды для работы с сервисом
### Установка
```
make install
```
### Запуск приложения
```
./manage.py api run
```
### Проверка кода
запуск линтеров
```
make lint
```
запуск форматеров
```
make format
```
запуск тестов 
```
make test
```
### Команды миграций
создание миграции 
```
./manage.py alembic makemigration
```
запуск миграции до последней ревизии
```
./manage.py alembic upgrade
```
запуск миграции до конкретной ревизии 
```
./manage.py alembic upgrade {name_revision}
```
откат миграции на предыдущую ревизию 
```
./manage.py alembic downgrade
```
откат миграции на конкретную ревизию
```
./manage.py alembic downgrade {name_revision}
```

### Запуск Gitlab Pipeline

Установка раннера пайплайнов 

```
make gitlab-ci-install
```

Локальный запуск GitLab Pipelines

```
make gitlab-ci-test
```


### Работа с Faust

Запуск Faust

```
make faust
```

Отправка сообщения

```
faust -A app.stream.agents send tool_action '{"task_id": "1", "task_name": "work/success_nb.ipynb", "tool_run_id": "1", "action": "run"}'
```
